# Release Notes

## 1.0.3 (2018-01-14)
Rebrand OnPoint to DevelopersWarehouse.

## 1.0.2 (2017-12-21)
  1. Table migration for `meta`.
  2. Added `ServiceProvider`.
  3. Updated constraints to Laravel `>=5.5`, for provider discovery.

## 1.0.1 (2017-12-20)
Update to package constraints

## 1.0 (2017-12-20)
Initial creation of package
