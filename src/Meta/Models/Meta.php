<?php

namespace DevelopersWarehouse\Meta\Models;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $table = 'meta';

    protected $guarded = ['id'];

    public function meta()
    {
        return $this->morphTo();
    }
}
