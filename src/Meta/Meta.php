<?php

namespace DevelopersWarehouse\Meta;

use DevelopersWarehouse\Meta\Models\Meta as MetaModel;

trait Meta
{
    public function meta()
    {
        return $this->morphMany(MetaModel::class, "meta");
    }

    public function metaByKey($key)
    {
        if ($this->meta()->where("key",$key)->count()) {
            return $this->meta()->where("key",$key)->get();
        }
        return null;
    }

    public function metaValueByKey($key)
    {
        if ($this->meta()->where("key",$key)->count()) {
            return $this->meta()->where("key",$key)->pluck("value");
        }
        return null;
    }

    /**
     * To quickly access a meta property for a model, use the ->meta{Key} attribute to access the value field.
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    public function __get($key)
    {
        if (starts_with($key, "meta") && $key != "meta") {
            $key = snake_case(str_after($key,"meta"));
            return $this->metaValueByKey($key);
        }

        return parent::__get($key);
    }

}
